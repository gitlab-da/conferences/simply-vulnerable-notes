---
bookCollapseSection: false
weight: 50
---

# Deploying an Application with GitLab Pipeline

In this lesson, we will import the demo project and deploy the complete application environment to our Kubernetes cluster. The application environment includes:

- Simple Notes (front-end application used for note-taking)
- MariaDB Backend (database to store notes)
- Echo Application (used as a static base url)

## Step 1: Importing the Sample Project

Here we will import the sample project which we will use through this workshop. It's a simple python Flask application which adds and removes notes from a MariaDB database.

1. Login to [GitLab.com](https://gitlab.com/users/sign_in)

2. On the left side-panel click on **Projects**, or you can navigate to [https://gitlab.com/projects/new](https://gitlab.com/projects/new)

3. Select **Import project**

4. Press the **Repo By URL** button

5. Under **Git repository URL** add the following URL:

```text
https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes.git
```

6. Under **Project URL**, select the group for which you have an Ultimate License. I am selecting my personal user group, `https://gitlab.com/fjdiaz`

7. Under **Visibility Level** select **Public**

{{< hint info >}}
**Note:** Public is set so that we don't need to take extra steps to be able to [pull
from a private container-registry](https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/)
{{< /hint >}}

8. Press the **Create project** button

9. Wait for the project to be imported. It will take a few seconds. You should be redirected to the newly imported project along with the message **The project was successfully imported**

### Step 2. Setting up the Project to use the Cluster

In this section we will be installing the GitLab [Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/) to interact with the cluster and deploy our Kubernetes manifests.

1. Click on the **Operate > Kubernetes clusters** in the left navigation menu

2. Click on the **Connect a cluster (agent)** button

3. Select the `simplenotes` agent from the drop down and press the **Register** button. Keep this window open

4. Open a terminal and connect to your cluster

```bash
$ gcloud container clusters get-credentials fern-simple-notes --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for fern-simple-notes.
```

5. **Copy and paste** the provided command to your terminal in order to deploy the agent onto your cluster:

```bash
$ helm repo add gitlab https://charts.gitlab.io
$ helm repo update
$ helm upgrade --install simplenotes gitlab/gitlab-agent \
    --namespace gitlab-agent-simplenotes \
    --create-namespace \
    --set image.tag=v16.0.1 \
    --set config.token=2kfSzYpGSUyxfHwbd_3LNgfz15YxM5ArGSsMjq4qsz1mNXsREg \
    --set config.kasAddress=wss://kas.gitlab.com

"gitlab" already exists with the same configuration, skipping
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "bitnami" chart repository
...Successfully got an update from the "gitlab" chart repository
Update Complete. ⎈Happy Helming!⎈

Release "simplenotes" does not exist. Installing it now.
NAME: simplenotes
LAST DEPLOYED: Tue May 16 12:42:58 2023
NAMESPACE: gitlab-agent-simplenotes
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

6. Verify the Kubernetes Agent is running

```bash
$ kubectl get pods -n gitlab-agent-simplenotes

NAME                                       READY   STATUS    RESTARTS   AGE
simplenotes-gitlab-agent-6949689c4-ps8m7   1/1     Running   0          37s
```

### Step 3: Running the Pipeline

Now let's run a pipeline to deploy the application to our Kubernetes cluster.

1. Click on the **Build** left navigation menu and click on **Pipelines**

2. Click on **Run Pipeline**

3. Ensure that the **main** branch is selected

4. Press the **Run Pipeline** button. The screen should refresh and you should now see the pipeline running on your project. The pipeline should look as follows:

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/pipeline_started.png)

### Step 4: Reviewing the Pipeline

Now let's wait for the pipeline to complete, this should take a few mins. Grab a coffee, tea, red-bull, etc. and take a little break, **you deserve it!**

{{< hint warning >}}
**Note:** If the pipeline happens to fail, please checkout the [troubleshooting documentation](../../documentation/troubleshooting).
{{< /hint >}}

1. Verify all jobs have completed successfully. It should look like the below:  

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/pipeline_completed.png)

## Step 5: Accessing our Application

Now let's use the ingress to access our application. With the default settings your application should be available at `http://LOAD-BALANCER-IP/notes-main`

1. Click on the `deploy-simple-notes` job and scroll to the bottom. You should see the URL which the application was deployed to. Click on that URL to open the application in your browser.

```bash
Access your application at http://35.188.163.131/notes-main
```

{{< hint info >}}
**Note:** You can also go to the `Build > Environments` side tab and click the `Open` button under the **main** environment.
{{< /hint >}}

2. You should now see the Simple Notes Application running, where you can now create/view/delete notes.

![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/running_application.png)

3. You can also create/view/delete secret notes in admin mode. To access admin mode, click the `Admin` button with the following credentials:

**username:**  admin  
**password:**  yeet

---

Congratulations, you have now successfully deployed an application using GitLab CI/CD! Now let's move on to the next lesson, setting up and configuring GitLab security scanners and policies.

{{< button relref="/lesson_2_tutorial_prerequisites" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_4_setting_up_and_configuring_the_security_scanners_and_policies" >}}Next Lesson{{< /button >}}