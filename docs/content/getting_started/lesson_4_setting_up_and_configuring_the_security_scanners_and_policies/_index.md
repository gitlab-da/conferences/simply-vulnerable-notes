---
bookCollapseSection: false
weight: 60
---

# Enabling and Configuring Security Scans and Policies

In this section, we will go over the security scans which GitLab offers. We will then setup all of the scans and run them on our main branch.

## What Security Scans does GitLab offer

GitLab offers a variety of security scans to enhance application security. Some scanners will scan the static source code, and others will scan the running application for vulnerabilities.

The scanners use both **OpenSource** and **GitLab built** scanners, which vary by language. The language in the application is auto-detected by GitLab. For the **OpenSource** scanners, the infrastructure is maintained by GitLab, and the rules used are created by our [Security Researchers](https://about.gitlab.com/handbook/security/threat-management/security-research/)

We will go over the following scanners:

1. [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/): analyzes your source code for known vulnerabilities. GitLab also provides [Advanced SAST](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html) which provides multi-file and multi-function analysis.
2. [Dynamic Application Security Testing (DAST)](https://docs.gitlab.com/ee/user/application_security/dast/): analyzes your running application for known vulnerabilities.
3. [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/): scans container images for known vulnerabilities
4. [Dependency + License Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/): scans project dependencies for known vulnerabilities and detects licenses used by dependencies
5. [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/): Scans for secrets checked into source code
6. [Infrastructure as Code Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/): Scans your IaC configuration files for known vulnerabilities. IaC scanning supports configuration files for Terraform, Ansible, AWS CloudFormation, and Kubernetes.
7. [Coverage-Guided Fuzzing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/): Sends random inputs to an instrumented version of your application in an effort to cause unexpected behavior.
8. [Web-API Fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/): Sets operation parameters to unexpected values in an effort to cause unexpected behavior and errors in the API backend
9. [DAST API-Scanning](https://docs.gitlab.com/ee/user/application_security/dast_api/): analyzes the APIs of your running application for known vulnerabilities using REST, SOAP, GraphQL, Form bodies, JSON, or XML definitions.
10. [Operational Container Scanning](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html): scans the container images in your cluster for known vulnerabilities.

## Step 1: Adding Security Scans to the pipeline

Security scanners can be added in 2 different ways. 

- Using the [Security Configuration UI](https://docs.gitlab.com/ee/user/application_security/configuration/#security-testing)
- Adding [CI templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates) to the [.gitlab-ci.yml](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/blob/main/.gitlab-ci.yml)

Security scanners have already been added to this project via templates. Below I'll explain how the security scanners work and separate them into 3 different categories:

- Static Security Scanners
- Dynamic Security Scanners
- Coverage-based and Web-API Fuzzers

### Static Security Scanners

Static security scanners examine the static source code in your project and perform pattern matching on syntax, versions, etc. in order find known vulnerabilities. They obtain the vulnerabilities from a CVE database and parse data in order to provide you with the following:

* Description
* Severity
* Project (may include line of code)
* Scanner type
* Evidence 
* Relevant links (Education/Training, Solutions)
* Identifiers (CVE, CWE)

### Dynamic Security Scanners

Dynamic scanners examine the running application, and send requests in order to find vulnerabilities within the system. Dynamic scanners are not aware of the underlying code, and perform requests blindly to the application.

{{< hint info >}}
**Note:** Since **requests** are sent to the application and **responses** are received, they are included along with the same data as static scanners (listed above). You can download Postman specs in order to replicate the **requests**, this is useful for manual testing.
{{< /hint >}}

### Application and Web-API fuzzers

Fuzzing or Fuzz-Testing is the process of sending **random** or **malformed** data to an application or instrumented function in order to cause unexpected behavior. This helps you discover bugs and potential security issues that other QA processes may miss.

GitLab includes Web-API Fuzzing (fuzz testing of API operation parameters) and Coverage-Guided Fuzzing (sends random inputs to an instrumented version of your application).

## Step 2: Explanation of each of the CI/CD job

There's a bunch of CI/CD jobs that do a bunch of different things, I'll briefly explain them below.

### Standard Jobs

- **build-simple-notes**: Builds the container image for using the application in Kubernetes
- **pages**: Build the documentation using [Go Hugo](https://gohugo.io/) Static Site Generator
- **unit**: Runs Unit Tests from the application
- **deploy-simple-notes**: Installs the application and it's dependencies to the added Kubernetes cluster
- **cleanup-db**: Resets notes which have been added via dynamic security scans

### Job Overwrites

- **dast**: Sets up different DAST scan types depending on the branch (ex. Passive on main, Active on other)
- **dast_api**: Overwrites paths used for running dast_api
- **apifuzzer_fuzz**: Overwrites paths used for running api-fuzzing
- **gemnasium-dependency_scanning**: Overwrites the pre_script of dependency scanning to install required system dependencies
- **coverage-guided-fuzzing**: Runs coverage-guided fuzzing on a provided instrumented file
- **kics-iac-sast**: Excludes certain paths from IaC scanning
- **secret_detection**: Excludes certain paths from Secret detection

## Step 3: Scanner Configurations

Each scanner can be configured using environment variables. Depending on the scanner there are different types of configurations which are possible. For example, if we want to configure our IaC scanner to **exclude certain paths** and **run during a different stage**, we can add the following:

```yaml
# Adds the kics-iac-sast to the pipeline
include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml

# Overwrites the job provided by a template
kics-iac-sast:
  # Sets up which stage this job will run on
  stage: deploy
  # Sets up new variables for the job
  variables:
    # Used to exclude paths from the scan
    SAST_EXCLUDED_PATHS: "spec, test, tests, tmp, terraform, scripts, network-policies"
```

Every scanner has different options available. See the [application security documentation](https://docs.gitlab.com/ee/user/application_security/) for information on each scanner.

{{< hint info >}}
**Note**: Pipelines are also highly configurable and additional rules can be applied to suit your needs. See [GitLab's rules documentation](https://docs.gitlab.com/ee/ci/jobs/job_control.html) for more information.
{{< /hint >}}

## Step 4: Enabling Security Training

GitLab provides security training to help your developers learn how to fix vulnerabilities. Developers can view security training from selected educational providers, relevant to the detected vulnerability.

{{< hint info >}}
**Note**: Security training is only displayed for certain vulnerabilities under the vulnerability report. See the [security training documentation](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#view-security-training-for-a-vulnerability) for more info.
{{< /hint >}}

1. Go to the the **Secure** left navigation menu and press **Security Configuration**  

2. Click on the **Vulnerability Management** tab

3. Check all the **Security training** providers

- **Kontra**: provides interactive developer security education that enables engineers to quickly learn security best practices and fix issues in their code by analyzing real-world software security vulnerabilities.
- **Secure Code Warrior**: Resolve vulnerabilities faster and confidently with highly relevant and bite-sized secure coding learning.
- **SecureFlag**: Get remediation advice with example code and recommended hands-on labs in a fully interactive virtualized environment.

## Step 5: Setting up Merge-Request Approval Policies (Vulnerabilities)

Code reviews are an essential part of every successful project. Approving a merge request is an important part of the review process, as it clearly communicates the ability to merge the change. 

GitLab provides security guard-rails to prevent vulnerable code from being merged without approval. These guardrails are known as [Scan Result Policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)

1. Go to the the **Secure** left navigation menu and press **Policies**  

2. Click on the  **New policy** button   

3. Press the **Select policy** button under the **Merge request approval policy** section

4. Fill out the following information:

- **Name:** require_maintainer_approval_for_detected_vulnerabilities  
- **Description (optional):** Requires approval from maintainers before vulnerable code can be merged

5. Check the **Enabled** radio button under **Policy status**

6. Policy scope is set to current project. You would be able to select various option if the policy was applied at the group-level.

7. Under the **Rules** section create a rule with the following specifications:

> When `Security Scan` `All scanners` runs against the `all protected branches` with `No exceptions` and find(s) `Any` vulnerability that match all of the following criteria:

> Severity is: `Select severity levels`

> Status is: `New` `All vulnerability states`

8. Under the **Actions** section create an action with the following criteria:

> Require `1` approval from `Roles` `Maintainer`

{{< hint info >}}
**Note:** You can also set **individual approvers** or **groups** as approvers, for example (the security team). If you want to learn more about role permissions. See the [GitLab Permissions and Roles documentation](https://docs.gitlab.com/ee/user/permissions.html)
{{< /hint >}}

9. Under Override project approval settings you can configure **Protected branch** and **Merge request approval** settings. Let's unselect all the default options.

10. Click on the **Configure with a merge request** button, you will be transported to a merge-request

{{< hint info >}}
**Note:** Notice that when creating a merge-request a new project is created with the following name
`<your-project-name>-security-policy-project`. This project will store all the policy files for your project.
{{< /hint >}}

11. Press the **Merge** button to enable the policy

## Step 6: Setting up Merge-Request Approval Policy (Licenses)

Now let's do the same thing, but for requiring approval for restrictive/incompatible licenses.

1. Go back to your project

2. Go to the the **Secure** left navigation menu and press **Policies**  

3. Click on the  **New policy** button   

4. Press the **Select policy** button under the **Merge request approval policy** section

5. Fill out the following information:

- **Name:** require_maintainer_approval_for_non_MIT_licenses  
- **Description (optional):** Requires approval from maintainers before non-MIT licensed dependencies can be merged

6. Check the **Enabled** radio button under **Policy status**

7. Under the **Rules** section create a rule with the following specifications:

> When `License Scan` in an open merge request targeting `all protected branches` with `No exceptions` and the licenses match all of the following criteria:

> Status is: `Newly Detected`

> License is: `Except` `MIT License`

8. Under the **Actions** section create an action with the following criteria

> Require `1` approval from `Roles` `Maintainer`

{{< hint info >}}
**Note:** You can also set **individual approvers** or **groups** as approvers, for example (the security team). If you want to learn more about role permissions. See the [GitLab Permissions and Roles documentation](https://docs.gitlab.com/ee/user/permissions.html)
{{< /hint >}}

9. Under Override project approval settings you can configure **Protected branch** and **Merge request approval** settings. Let's unselect all the default options.

10. Click on the **Update with a merge request** button, you will be transported to a merge-request

{{< hint info >}}
**Note:** Notice that when creating a merge-request, the new policy is appended to the `policy.yaml` in the new project `<your-project-name>-security-policy-project`. This project was created with the first policy added.
{{< /hint >}}

11. Press the **Merge** button to enable the policy

## Step 7: Injecting Custom Job using Pipeline Execution Policies

Now let's create a Pipeline execution policy which allows us to enforce a custom CI/CD configuration to run in project pipelines. This is useful for creating and applying custom compliance checks.

1. Go back to your project

2. Go to the the **Secure** left navigation menu and press **Policies**  

3. Click on the  **New policy** button   

4. Press the **Select policy** button under the **Pipeline execution policy** section

5. Fill out the following information:

- **Name:** inject_custom_SOC2_echo_job  
- **Description (optional):** Injects a custom compliance job to echo 'SOC2 Compliance Check' in our project

6. Check the **Enabled** radio button under **Policy status**

7. Under the **Actions** section create an action with the following criteria

> `Inject` into the .gitlab-ci.yml with the following pipeline execution file from `Custom Jobs`

> File path: .../devsecops/custom-jobs `soc2.yml`

> File reference (Optional) `default branch`

> Add job name suffix `On conflict`

{{< hint info >}}
**Note:** The `Custom Jobs` project may not be selectable in from your project. You can manually add it by typing in `gitlab-da/tutorials/security-and-governance/devsecops/custom-jobs` or you can clone the [Custom Jobs](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/custom-jobs) project into your group.
{{< /hint >}}

8. Click on the **Update with a merge request** button, you will be transported to a merge-request

{{< hint info >}}
**Note:** Notice that when creating a merge-request, the new policy is appended to the `policy.yaml` in the new project `<your-project-name>-security-policy-project`. This project was created with the first policy added.
{{< /hint >}}

9. Press the **Merge** button to enable the policy

---

Congratulations, you have now successfully run security scans, enables educational resources and AI, and setup security guardrails for your application! Now let's move on to actually seeing and taking action on the vulnerabilities.

{{< button relref="/lesson_3_deploying_the_demo_application" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_5_developer_workflow" >}}Next Lesson{{< /button >}}