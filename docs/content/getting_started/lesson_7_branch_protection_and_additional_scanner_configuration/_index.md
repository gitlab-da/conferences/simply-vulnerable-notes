---
bookCollapseSection: false
weight: 90
---

# Additional Security Protections and Additional Scanner Configuration (Optional)

The following section provides information on how to add merge-request approvals, branch protections,
and add additional configurations for security scanners. These configurations include DAST profiles,
scan enforcement policies, as well as custom rule-sets for our scanners.

## Step 1: Setting up additional merge-request approvals

In this section I will be going over some additional approval protections GitLab provides.

1. Go to the the **Settings** left navigation menu and press **Merge requests**  

2. Scroll down to the **Merge checks** section. I'm not going to select any, but you can see the additional setting that you can select are as follows:

- **Pipelines must succeed**: Merge requests can't be merged if the latest pipeline did not succeed or is still running.
- **Skipped pipelines are considered successful**: Introduces the risk of merging changes that do not pass the pipeline.
- **All threads must be resolved**: No threads can be un-resolved in order to merge.
- **Status checks must succeed**: Merge requests can't be merged if the status checks did not succeed or are still running.

{{< hint info >}}
**Note:** Status checks are API calls to external systems that request the status of an external requirement. Status checks are outside the scope of this tutorial, but you can learn more by visiting the [Status Check documentation](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html)
{{< /hint >}}

3. Scroll down to the **Merge request approvals** section

4. Under **Approval rules**, you can see the **Coverage-Check**. If you enable this, then any drop in coverage will require approval from the selected members.

5. Scroll down to **Approval settings**, here you can see additional setting to protect your source code which include:

- Prevent approval by author
- Prevent approvals by users who add commits
- Prevent editing approval rules in merge requests
- Require user password to approve

You can also provide different actions whenever a commit is added, such as:

- Keep approvals
- Remove all approvals
- Remove approvals by Code Owners if their files changed

6. Check **Prevent approvals by users who add commits**

7. Press the **Save Changes** button. Now your changes have been applied

## Step 2: Setting up additional branch protections

In this section I will be going over some [branch protection rules](https://docs.gitlab.com/ee/user/project/protected_branches.html) GitLab provides.

1. Go to the the **Settings** left navigation menu and press **Repository**  

2. Scroll down to the **Push rules** section and press **Expand**

Here you can enable the following push rules:

- **Reject unverified users**: Users can only push commits to this repository if the committer email is one of their own verified emails.
- **Reject unsigned commits**: Only signed commits can be pushed to this repository.
- **Reject commits that aren't DCO certified**: Only commits that include a Signed-off-by element can be pushed to this repository.
- **Do not allow users to remove Git tags with git push**: Users can still delete tags through the GitLab UI.
- **Check whether the commit author is a GitLab user**: Restrict commits to existing GitLab users.
- **Prevent pushing secret files**: Reject any files likely to contain secrets. What secret files are rejected?

3. Scroll down to the **Protected branches** section and press **Expand**. Protected branches keep stable branches secure and force developers to use merge requests. Here you can set the following:

- **Branch**: Branch or wildcard which rules will be applied to.
- **Who is allowed to merge**: Roles or Users who can merge to branch.
- **Allowed to push and merge**: Roles or Users who can push and merge to branch.
- **Allowed to force push**: Allow all users with push access to force push.
- **Require approval from code owners**: Reject code pushes that change files listed in the CODEOWNERS file.

{{< hint info >}}
**Note:** CodeOwners allows you to define who has expertise for specific parts of your project’s codebase, and setup who must approve a change for a certain **file/file-type/directory**. This is out of the scope of this tutorial, but feel free to learn more by checking out the [CodeOwners documentation](https://docs.gitlab.com/ee/user/project/codeowners/)
{{< /hint >}}

## Step 3: Customize Rulesets for secret detection

GitLab uses rules defined in [GitLeaks](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml) for secret detection. You can customize the default Secret Detection rules provided with GitLab. The following customization options can be used separately, or in combination:

- [Disable predefined rules](https://docs.gitlab.com/ee/user/application_security/secret_detection/#disable-predefined-analyzer-rules)
- [Override predefined rules](https://docs.gitlab.com/ee/user/application_security/secret_detection/#override-predefined-analyzer-rules)
- [Synthesize a custom configuration](https://docs.gitlab.com/ee/user/application_security/secret_detection/#synthesize-a-custom-configuration)

You can see an example of a ruleset configuration in this project under ['.gitlab/secret-detection-ruleset.toml'](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/blob/main/.gitlab/secret-detection-ruleset.toml):

```js
[secrets]
  description = 'Hardcoded Credential Detection Override'

  [[secrets.passthrough]]
    type  = "raw"
    target = "gitleaks.toml"
    value = """\
# Overwrites GitLeaks Config
title = "gitleaks config"

# Detects password variations
[[rules]]
description = "Possible Hardcoded Variations of 'Password'"
regex = '''[Pp][a-zA-Z@][sS$][sS$][wW][oO0][rR][dD].*'''

# Detects the word admin
[[rules]]
description = "Possible Hardcoded Username - 'Admin'"
regex = '''[Aa]dmin'''
"""
```

This example uses passthroughs to override the default Secret Detection ruleset and adds detection for the following patterns:

- [Pp][a-zA-Z@][sS$][sS$][wW][oO0][rR][dD].*: detects any variation of the word password, such as **p@s$w0rd1**
- [Aa]dmin: detects the word **admin** with a capital or lowercase a

Now whenever the scanner is run, it will run with the above rules. Note that all other (default) rules are ignored, however you can [extend the default rulesets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#extending-the-default-configuration) if you wish.

{{< hint info >}}
**Note**: GitLab also allows you to [customize the behavior of it's SAST analyzers](https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html) by defining a ruleset configuration file in the repository being scanned.
{{< /hint >}}

## Step 4: Forcing scans to run with Scan Execution Policies

GitLab provides scan execution policies which force security scans to run regardless if they are configured in the `.gitlab-ci.yml`. This enables separation of duties by only allowing those with maintainer access to be able to remove security scanners from running. Scans can also be scheduled to run on a schedule.

1. Go to the the **Secure** left navigation menu and press **Policies**  

2. Click on the  **New policy** button   

3. Press the **Select policy** button under the **Scan execution policy** section

4. Fill out the following information:

- Name: Policy Name
- Description: Policy Description

5. Check the **Enabled** radio button under **Policy status**

6. Under the **Actions** section create an action with the following specifications:

> Run a `SAST` scan on runner that `selected automatically`

7. Under the **Conditions** section create a condition with the following criteria:

> `Schedules:` `daily` at `00:00` for `branch` `main`

{{< hint info >}}
**Note:** The schedule is evaluated in standard [UTC](https://www.timeanddate.com/worldclock/timezone/utc) time from GitLab.com. If you have a self-managed GitLab instance and have changed the server time zone, the schedule is evaluated with the new time zone.
{{< /hint >}}

8. Click on the **Configure with a merge request** button, you will be transported to a merge-request

{{< hint info >}}
**Note:** Notice that when creating a merge-request, the new policy is appended to the `policy.yaml` in the new project `<your-project-name>-security-policy-project`. This project was created with the first policy added.
{{< /hint >}}

9. Press the **Merge** button to enable the policy

10. Wait until the time the scanner was configured to run. This maybe tomorrow depending on what time you configured the scanner to run at.

11. Go to the the **Build** left navigation menu and press **Pipelines**

12. Click on the pipeline which ran only the SAST job

## Step 5: Configuring and running DAST on-demand

Dynamic Application Security Testing (DAST) examines applications for vulnerabilities like these in deployed environments. We looked at the basics of DAST in the earlier sections, but here we will see the additional configurations available.

1. Go to the the **Secure** left navigation menu and press **On-demand scans**  

2. Press the **New scan** button

3. Fill out the following information:

- **Scan Name**: New of the scan to perform
- **Description**: Description of the scan that will be performed
- **Branch**: The branch the scan will run on

Now we will proceed to configure our DAST scanner with the following:

- **Scanner profile**: defines the configuration details of a security scanner
- **Site Profile**: defines the attributes and configuration details of your deployed application, website, or API

### Creating a Scanner profile

1. Scroll down to the **DAST configuration** section

2. Press the **Select scanner profile** button

3. Press the **New scanner profile** button

4. Add the following criteria:

- **Profile name**: Add a name for the profile
- **Scan mode**: Active or Passive - For this tutorial it will be Passive
- **Spider timeout**: Minutes for spider to traverses site for
- **Target timeout**: Seconds for target site to respond
- **AJAX spider**: Runs the AJAX spider along with the traditional spider
- **Debug messages**: Add debug messages to output

5. Press the **Save profile** button. You should now see the **Scanner profile** you created in the UI

### Creating a Site profile

1. In the **DAST configuration** section press the **Select site profile** button

2. Press the **New site profile** button

3. Add the following criteria:

- **Profile name**: Add a name for the profile
- **Site type**: Website or API

If you select **Website** as the site profile, you must configure the following:

- **Target URL**: The URL of the website - For this tutorial it will be http://LOAD-BALANCER-IP/notes-main
- **Excluded URLs**: URLs to skip during the authenticated scan
- **Additional Request Headers**: Headers to add to DAST requests
- **Authentication**: Allows you to submit the Authentication URL, Username, Password, Username form field, Password from field, and Submit button in order to run DAST within Authenticated pages

If you select **API** as the site profile, you must configure the following:

- **API endpoint URL**: The endpoint of the API - For this tutorial it would be http://LOAD-BALANCER-IP/notes-main
- **Scan method**: HTTP Archive (HAR), GraphQL, OpenAPI, Postman Collection - For this tutorial it would be OpenAPI
- **Specification file URL**: Where the OpenAPI, Postman, GraphQL, or HAR file is located - For this tutorial it would be https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/raw/main/test_openapi.v2.0.json
- **Excluded Paths**: Paths to skip during the authenticated scan
- **Additional Request Headers**: Headers to add to DAST requests

6. Press the **Save profile** button. You should now see the **Site profile** you created in the UI

{{< hint warning >}}
**Note:** If you selected Active as the scan mode within the scanner profile, you will need to validate the site that you are scanning. For more information on validating a site see the [Site Validation Documentation](https://docs.gitlab.com/ee/user/application_security/dast/proxy-based.html#validate-a-site-profile).
{{< /hint >}}

### Scheduling the scan

1. Scroll down to the **Scan Schedule** section

2. Press the **Enable scan schedule** button

3. Add the following criteria:

- **Start time**: When to start the scan
- **Timezone**: Schedule timezone
- **Repeats**: How many times a scan will repeat. (daily, weekly, monthly, quarterly, every 6 months, yearly)

### Running the scan

1. Scroll down to the bottom of the page

2. Press the **Save and run scan** button

3. You should now see DAST running by itself with the profiles you provided. It will run on the schedule your provided.

{{< hint info >}}
**Note:** You can view all the DAST scans which have been run, see the results, and change schedules by going back to the **Secure > On-demand scans** section
{{< /hint >}}

---

Congratulations, you are now able to add branch protections, configure Secret Detection + DAST, and enable separation of duties! Now let's explore enabling/configuring network policies using GitOps.

{{< button relref="/lesson_6_appsec_workflow" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_8_policy_as_code_gitops" >}}Next Lesson{{< /button >}}