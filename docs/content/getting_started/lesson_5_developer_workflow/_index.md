---
bookCollapseSection: false
weight: 70
---

# Developer Workflows

In this lab, we will go over how Vulnerabilities can be viewed as well as the information and actions available to a user. We are going to add some vulnerable code to a feature branch and then the scanners will run and display the found vulnerabilities.

## Step 1: Adding Vulnerable Code

Now let's go ahead and add some vulnerabilities. We will make sure that something can be picked up by each type of scanner.

1. Open the **WebIDE** from the project page

{{< hint info >}}
**Note:** To learn more about the GitLab Web IDE and how to use/configure it, checkout the
[Web IDE documentation](https://docs.gitlab.com/ee/user/project/web_ide/)
{{< /hint >}}

2. Copy over the changes found in [this Merge Request](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/merge_requests/1)

{{< hint warning >}}
**Note:** I'll try to keep it up-to-date and re-based, if it isn't open up an issue within the project
{{< /hint >}}

3. Click on the **Source Control** Tab in the side-panel of the WebIDE. It is the [3rd icon from the top](https://code.visualstudio.com/docs/sourcecontrol/overview#_commit)

4. Click on the **⌄** button to the right of **Commit to 'main'**

5. Click **Commit to new branch**

6. Enter a **branch name** and press enter

7. On the bottom right of the screen a popup will appear, click on the **Create MR** button

{{< hint info >}}
**Note:** If you missed the popup you can create a merge request from the project's
[merge request tab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).
{{< /hint >}}

8. Scroll down through the MR template and click on the **Create merge request** button. This will trigger a new pipeline. This pipeline looks a little different than the one we saw previously on the main branch. It should look as follows:

!![](/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/images/complete_mr_pipeline.png)

It runs the additional jobs which aren't run on the main branch (production) because they can be destructive in nature:

- dast_api
- apifuzzer_fuzz
- cleanup-db

9. Wait for the pipeline to complete. This will take a while, so use this time to take a little break and get a good stretch in

{{< hint error >}}
If the pipeline happens to fail, please checkout the [troubleshooting documentation](../../documentation/troubleshooting)
{{< /hint >}}

## Step 2: Viewing the Security Guardrails

1. Go to the merge request you created in Step one

2. Expand the **Requires 2 approvals** tab

3. You should see that the policies we created are active and that approval is required since vulnerabilities and incompatible licenses were detected

## Step 3: Viewing and taking action on vulnerable code

Now we can view the vulnerabilities after the pipeline started above has completely run.
Let's dig into the vulnerabilities and perform some actions on them.

1. Within the merge request, press **Expand** in the **Security scanning** section

2. Scroll through the vulnerabilities and notice how each vulnerability is sorted by its scanner

3. Click on any of the detected vulnerabilities and a popup will open with additional info

4. Within the popup, dismiss the Vulnerability by clicking the **Dismiss vulnerability** button

{{< hint info >}}
**Note:** This allows AppSec teams to see what developers are dismissing as well as why. If this MR were to be merged, then the vulnerability will automatically be tagged as dismissed in the vulnerability report
{{< /hint >}}

5. Click on the same vulnerability to see the popup again

6. Click on **Create issue**  

{{< hint info >}}
**Note:** This creates a confidential issue to allow developers and the security team to
work together to resolve a vulnerability without displaying it to others (possibly malicious actors)
{{< /hint >}}

7. Now go back to the Merge Request by pressing the **back button** on your browser

## Step 4: Viewing Denied Licenses

Within the same MR view, we can see the licenses that were detected. You'll be able to see which licenses are approved and denied according to the policy we set in an earlier lab.

1. Within the merge request, press **Expand** button in the **License Compliance** section.

2. Look over all the detected licenses which are shown as **Denied** since they are not **MIT** licenses

## Step 5: Viewing Injected Compliance Jobs

Now lets take a look at the custom compliance job we injected.

1. Click on the pipeline icon

2. Select the `soc2_compliance_check` job in the `.pipeline-policy-post` stage

3. See the echo output "SOC2 Compliance Check" which is injected from a separate yaml file containing separate permissions.

---

Congratulations, you have now successfully viewed vulnerabilities within an MR and the details to their resolution! Now lets move on to some appsec workflows such as managing vulnerabilities.

{{< button relref="/lesson_4_setting_up_and_configuring_the_security_scanners_and_policies" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_6_appsec_workflow" >}}Next Lesson{{< /button >}}
