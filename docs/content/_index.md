---
title: Introduction
type: docs
---

# Welcome

Welcome to Simple Notes - GitLab DevSecOps Introduction. This Project will help you gain a better understanding of how to successfully shift security left to find and fix security flaws during development and do so more easily, with greater visibility and control than typical approaches can provide.

## Getting Started

In order to get started, go through each of the lessons described within the workshop:

- [Lesson 1: DevSecOps Overview](./getting_started/lesson_1_devsecops_overview/)  
- [Lesson 2: Tutorial Prerequisites](./getting_started/lesson_2_tutorial_prerequisites/)  
- [Lesson 3: Deploying the Demo Application](./getting_started/lesson_3_deploying_the_demo_application/)  
- [Lesson 4: Setting up and Configuring the Security Scanners and Policies](./getting_started/lesson_4_setting_up_and_configuring_the_security_scanners_and_policies/)  
- [Lesson 5: Developer Workflow](./getting_started/lesson_5_developer_workflow/)  
- [Lesson 6: AppSec Workflow](./getting_started/lesson_6_appsec_workflow/)  

This concludes the main portion of the tutorial. The rest of the tutorial is optional, but does
go over useful features, such as branch protections, additional configurations and scheduling for the security
scanners, gitops, and more. 

- [Lesson 7: Branch Protection and Additional Scanner Configuration (Optional)](./getting_started/lesson_7_branch_protection_and_additional_scanner_configuration)  
- [Lesson 8: Policy as Code with GitOps (Optional)](./getting_started/lesson_8_policy_as_code_gitops/)  
- [Lesson 9: Looking Forward (Optional)](./getting_started/lesson_9_looking_forward/)  

## Outcomes

- How to achieve comprehensive security scanning without adding a bunch of new tools and processes
- How to secure your cloud native applications and IaC environments within existing DevOps workflows
- How to use a single-source-of-truth to improve collaboration between dev and sec
- How to manage all of your software vulnerabilities in one place
- How to automate and monitor your security policies and simplify auditing
- How to detect unknown vulnerabilities and errors using fuzz-testing
- How to configure the security scanners and make them run on a schedule
- How to enable separation of duties and adhere to compliance
- How to perform GitOps and deploy to a Kubernetes cluster

## Sections

| # |     Title     |                Description                   |
| - |---------------|----------------------------------------------|
| 1 | DevSecOps Overview | Goes over the basics of DevSecOps and it's benefits |
| 2 | Prerequisites | Requirements to get started with the project |
| 3 | Deploying the Demo Application | Learn how to deploy and expose the demo application |
| 4 | Setting up and Configuring the Security Scanners and Policies | Learn how to setup and configure the different types of security scans. This includes Security Policies as well |
| 5 | Developer Workflow | Learn how to view and take action on vulnerabilities within a Merge Request |
| 6 | AppSec Workflow | Learn how to triage vulnerabilities and collaborate with other members of a Security team |
| 7 | Branch Protection and Additional Scanner Configuration | Shows how to configure branch protections rules, force scanners to run on a schedule, and additional scanner configurations |
| 8 | Policy as Code with GitOps | Leverage GitOps to automate kubernetes deployments |
| 9 | Looking Forward | What's next with GitLab Security |

## Additional Resources

To learn about the project we are using you can see the following documentation:

- [Project Architecture](./documentation/architecture/)
- [Development Guide](./documentation/development_guide/)
- [Contributing](./documentation/contributing/)